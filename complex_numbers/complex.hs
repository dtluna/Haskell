cConj :: (Num a) => (a, a) -> (a, a)
cConj (r,i) = (r, -i)

cAdd :: (Num a) => (a, a) -> (a, a) -> (a, a)
(r1,i1) `cAdd` (r2,i2) = (r1+r2, i1+i2)

cSub :: (Num a) => (a, a) -> (a, a) -> (a, a)
(r1,i1) `cSub` (r2,i2) = (r1-r2, i1-i2)

cMul :: (Num a) => (a, a) -> (a, a) -> (a, a)
(r1,i1) `cMul` (r2,i2) = (r1*r2 - i1*i2, r1*i2 + r2*i1)

cEqual :: (Num a, Eq a) => (a, a) -> (a, a) -> Bool
x `cEqual` y = x == y

cAbs :: (Num a, Floating a) => (a,a) -> a
cAbs (r,i) = sqrt(r^2 + i^2)

cDiv :: (Num a, Eq a, Fractional a, Floating a) => (a, a) -> (a, a) -> (a, a)
(r1, 0) `cDiv` (r2, 0) = (r1/r2, 0)
x `cDiv` y = (x `cMul` (cConj y)) `cDiv` (cAbs y, 0)
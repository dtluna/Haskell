cosines :: (Num a, Floating a) => [a] -> [a]
cosines = map cos

main = do
	print (cosines [pi, -pi, 0])